/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libralyproject;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class LibralyProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String url = "jdbc:sqlite:./db/library.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect Database");
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Statement stm =conn.createStatement();
            String sql = "SELECT userId,\n" +
                        "       loginName,\n" +
                        "       password,\n" +
                        "       name,\n" +
                        "       surname,\n" +
                        "       typeId\n" +
                        "  FROM user";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()){
                System.out.println(rs.getInt("userId")+" "+rs.getString("loginName"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null)
                conn.close();
                System.out.println("Close Database");
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
